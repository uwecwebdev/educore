(function() {
	if(eduCore.isLargeScreen()) {
		eduCore.ui = {
			mainBackgroundImage : 'images/home_ipad.png',
			dashboardHeight : 340,
			dashboardWidth : 612,
			mainBackgroundImageLandscape : 'images/home_ipad_l.png',
			dashboardHeightLandscape : 500,
			dashboardWidthLandscape : 410
		};
	} else if(eduCore.isHighDensityScreen()) {
		eduCore.ui = {
			mainBackgroundImage : 'images/home@2x.png',
			dashboardHeight : 170,
			dashboardWidth : 306,
			mainBackgroundImageLandscape : 'images/home@2x_l.png',
			dashboardHeightLandscape : 170,
			dashboardWidthLandscape : 306
		};
	} else {
		eduCore.ui = {
			mainBackgroundImage : 'images/home.png',
			dashboardHeight : 170,
			dashboardWidth : 306,
			mainBackgroundImageLandscape : 'images/home_l.png',
			dashboardHeightLandscape : 260,
			dashboardWidthLandscape : 220
		};
	}

	if(eduCore.isAndroid()) {
		eduCore.ui.backgroundSelectedProperty = 'backgroundSelected';
	} else {
		eduCore.ui.backgroundSelectedProperty = 'selectedBackground';
	}

	eduCore.extend(eduCore.ui, {
		backgroundSelectedColor : '#999',
		tabBarHeight : 36
	});
})();
