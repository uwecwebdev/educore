/*
 * 
 * X-Platfrom Navigation controller
 * Author: Kevin Whinnery - Appcelerator
 * original source
 * https://github.com/appcelerator-developer-relations/Forging-Titanium/tree/master/ep-002
 * 
 */
exports.NavigationController = function() {
	this.windowStack = [];
};

exports.NavigationController.prototype.open = function(/*Ti.UI.Window*/windowToOpen) {
	//add the window to the stack of windows managed by the controller
	this.windowStack.push(windowToOpen);

	//grab a copy of the current nav controller for use in the callback
	var that = this;
	windowToOpen.addEventListener('close', function() {
		that.windowStack.pop();
	});

	//hack - setting this property ensures the window is "heavyweight" (associated with an Android activity)
	windowToOpen.navBarHidden = windowToOpen.navBarHidden || false;

	//This is the first window
	if(this.windowStack.length === 1) {
		if(eduCore.isAndroid()) {
			windowToOpen.exitOnClose = true;
			windowToOpen.open();
		} else {
			this.navGroup = Ti.UI.iPhone.createNavigationGroup({
				window : windowToOpen
			});
			var containerWindow = Ti.UI.createWindow();
			containerWindow.add(this.navGroup);
			containerWindow.open();
		}
	}
	//All subsequent windows
	else {
		if(eduCore.isAndroid()) {
			windowToOpen.open();
		} else {
			this.navGroup.open(windowToOpen);
		}
	}
};


// Adds Support for custom back button navigation injected into navController
exports.NavigationController.prototype.close = function(/*Ti.UI.Window*/ windowToClose) {
	// need to close the window on close the open function will pop it off the stack
	this.navGroup.close(windowToClose);
};

//go back to the initial window of the NavigationController
exports.NavigationController.prototype.home = function() {
	//store a copy of all the current windows on the stack
	var windows = this.windowStack.concat([]);
	for(var i = 1, l = windows.length; i < l; i++) {
		if (this.navGroup) { this.navGroup.close(windows[i]); } else { windows[i].close(); }
	}
	this.windowStack = [this.windowStack[0]]; //reset stack
};
