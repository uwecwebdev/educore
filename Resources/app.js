// this sets the background color of the master UIView (when there are no windows/tab groups on it)
Titanium.UI.setBackgroundColor('#fff');

// monkeypatch require seems to not be needed iOS wise
require('lib/require').monkeypatch(this);

/*
 * create eduCore namespace
 */
var eduCore = {
	android: {
		menu: {}	
	},
	datetime: {},
    ui: {},
    __isLargeScreen: undefined,
    __isAndroid: undefined,
    controller: undefined
};

/*
 * globally add base UI and core utilities
 */
Ti.include(
	'educore/utilities.js',
	'educore/ui.js');

(function(){
	var NavigationController = require('lib/NavigationController'),
	AppWindow = require('ui/AppWindow');
	
	// create a global navigation controller
	eduCore.controller = new NavigationController();
	
	//open the base window
	eduCore.controller.open(new AppWindow());
})();

