
exports.HtmlWindow = function (args) {
    var instance = Titanium.UI.createWindow({
        id: 'htmlWindow',
        title: args.title,
        backgroundColor: '#FFF',
        navBarHidden: false,
        barColor: '#414444',
        width: '100%',
        height: '100%'
    });
    var webview = Ti.UI.createWebView({
        url: args.url,
        width: '100%',
        height: '100%'
    });
    instance.add(webview);
    
    instance.addEventListener('focus', function (e) {
        webview.url = args.url;
        webview.height = '100%';
        webview.width = '100%';
    });

    return instance;
};