/*
 * Looking into why android fails inner requires
 * without the patch re added
 */
require('lib/require').monkeypatch(this);

/*
 * initialize window
 */
exports.AppWindow = function() {

	var instance = Ti.UI.createWindow({
		backgroundImage : eduCore.ui.mainBackgroundImage,
		title : 'home',
		navBarHidden : true,
		exitOnClose : true
	});

	var dashboard = Ti.UI.createView({
		height : eduCore.ui.dashboardHeight,
		width : eduCore.ui.dashboardWidth,
		bottom : (eduCore.isLargeScreen()) ? 150 : 75,
		borderRadius : 0,
		layout : 'horizontal'
	});

	var iconHeight = 85;
	var iconWidth = 102;
	var imageSuffix = '';
	/*
	 * test for a large sreen
	 */
	if(eduCore.isLargeScreen()) {
		iconHeight = 170;
		iconWidth = 204;
		imageSuffix = '@2x';
	}
	/*
	 * define main window icons/objects
	 */
	var icons = [{
		image : 'images/dashboard/rss' + imageSuffix + '.png',
		func : "ui/RssWindow",
		args : {
			url : "http://www.uwosh.edu/today/feed",
			title : 'UW Oshkosh Today'
		}
	}, {
		image : 'images/dashboard/map' + imageSuffix + '.png',
		func : "ui/BasicWindow"
	}, {
		image : 'images/dashboard/comment' + imageSuffix + '.png',
		func : "ui/TwitterWindow"
	}];

	for(var i = 0; i < icons.length; i++) {
		var view = Ti.UI.createView({
			backgroundImage : icons[i].image,
			top : 0,
			height : iconHeight,
			width : iconWidth
		});
		view.func = icons[i].func;
		view.args = icons[i].args;

		view.addEventListener('click', function(e) {
			var iconWindow = require(this.func);
			eduCore.controller.open(new iconWindow(this.args));

		});
		dashboard.add(view);
	}

	Ti.Gesture.addEventListener('orientationchange', function(ev) {
		if(eduCore.isLandscape(ev.orientation)) {
			// Update your UI for landscape orientation
			instance.backgroundImage = eduCore.ui.mainBackgroundImageLandscape;
			dashboard.height = eduCore.ui.dashboardHeightLandscape;
			dashboard.width = eduCore.ui.dashboardWidthLandscape;
			dashboard.left = (eduCore.isLargeScreen()) ? 40 : 20;
			dashboard.bottom = (eduCore.isLargeScreen()) ? 130 : 20;
		} else {
			// Update your UI for portrait orientation
			instance.backgroundImage = eduCore.ui.mainBackgroundImage;
			dashboard.height = eduCore.ui.dashboardHeight;
			dashboard.width = eduCore.ui.dashboardWidth;
			dashboard.left = (eduCore.isLargeScreen()) ? 40 : 10;
			dashboard.bottom = (eduCore.isLargeScreen()) ? 150 : 75;
		}
	});

	instance.add(dashboard);

	return instance;
};
