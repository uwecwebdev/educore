exports.HeaderRow = function(title) {
	var headerRow = Ti.UI.createTableViewRow({
		classname : 'header_row',
		height : 26,
		backgroundImage : 'images/timebreak_gray@2x.png',
		selectedBackgroundImage : 'images/timebreak_gray@2x.png',
		touchEnabled : false
	});
	var headerLabel = Ti.UI.createLabel({
		text : title,
		color : '#fff',
		font : {
			fontSize : 16,
			fontWeight : 'bold'
		},
		left : 10,
		touchEnabled : false
	});
	headerRow.add(headerLabel);

	return headerRow;
};
