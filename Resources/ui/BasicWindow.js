
exports.BasicWindow = function() {
	var instance = Titanium.UI.createWindow({
		id : 'BasicWindow',
		title : 'Basic Window',
		backgroundColor : '#666',
		barColor : '#414444'
	});
	
	var label = Titanium.UI.createLabel({
		color:'#999',
		text:'I am a Basic Window',
		font:{fontSize:20,fontFamily:'Helvetica Neue'},
		textAlign:'center',
		width:'auto'
	});
	
	instance.add(label);
	
	return instance;
};
