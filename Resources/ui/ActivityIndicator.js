/*
 * Class: ActivityIndicator
 *   builds a transparent window that can be used to show and hide a loading message
 *   
 * 
 */
 

exports.ActivityIndicator = function() {
	this.isShowing = false;
	this.myTimeout = 11000;

	if(eduCore.isAndroid()) {
		this.activityIndicator = Ti.UI.createActivityIndicator({
			color : '#fff'
		});
	} else {
		this.activityIndicator = Ti.UI.createWindow({
			modal : false,
			navBarHidden : true,
			touchEnabled : true
		});
		
		var view = Ti.UI.createView({
			backgroundColor : '#000',
			height : '100%',
			width : '100%',
			opacity : 0.65
		});
		var ai = Ti.UI.createActivityIndicator({
			style : Titanium.UI.iPhone.ActivityIndicatorStyle.BIG,
			color : '#fff'
		});
		this.activityIndicator.ai = ai;
		this.activityIndicator.add(view);
		this.activityIndicator.add(ai);
	}
};

exports.ActivityIndicator.prototype.showModal = function(message, timeout, timeoutMessage) {
	if(this.isShowing) {
		return;
	}
	this.isShowing = true;
	if(eduCore.isAndroid()) {
		this.activityIndicator.message = message;
		this.activityIndicator.show();
	} else {
		this.activityIndicator.ai.message = message;
		this.activityIndicator.ai.show();
		this.activityIndicator.open({
			animated : false
		});
	}

	if(timeout) {
		this.myTimeout = setTimeout(function() {
			this.activityIndicator.hideModal();
			if(timeoutMessage) {
				var alertDialog = Ti.UI.createAlertDialog({
					title : 'Update Timeout',
					message : timeoutMessage,
					buttonNames : ['OK']
				});
				alertDialog.show();
			}
		}, timeout);
	}
};

exports.ActivityIndicator.prototype.hideModal = function() {
		if(this.myTimeout !== undefined) {
			clearTimeout(this.myTimeout);
			this.myTimeout = undefined;
		}
		if(this.isShowing) {
			this.isShowing = false;
			if(eduCore.isAndroid()) {
				this.activityIndicator.hide();
			} else {
				this.activityIndicator.ai.hide();
				this.activityIndicator.close({
					animated : false
				});
			}
		}
	};

