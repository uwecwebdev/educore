
exports.RssWindow = function(args) {
	var instance = Titanium.UI.createWindow({
		id : 'RssWindow',
		title : args.title,
		backgroundColor : '#FFF',
		barColor: '#414444'
	});
	// add optional custom back button for the home view
	if (!eduCore.isAndroid()) {
        var leftButton = Ti.UI.createButton({
            backgroundImage: 'images/6dots.png',
            width: 41,
            height: 30
        });
        leftButton.addEventListener('click', function () {
            eduCore.controller.close(instance, {
                animated: true
            });
        });
        instance.leftNavButton = leftButton;
    }
	var tableview = Titanium.UI.createTableView({
		backgroundColor : 'transparent',
		layout : 'vertical'
	});
	instance.add(tableview);
	
	function createRss() {
		// create table view data object
		var data = [];
		
		// startup activity indicator
		var ActivityIndicator = require("ui/ActivityIndicator");
		var ai = new ActivityIndicator();
		ai.showModal('Loading RSS feed...', 11000, 'The RSS feed timed out.');
		
		var xhr = Ti.Network.createHTTPClient();

		xhr.open("GET", args.url);

		xhr.onerror = function(e) {
			Ti.API.info(e);
		};
		
		xhr.onload = function() {
			try 
			{
				var doc = this.responseXML.documentElement;
				var items = doc.getElementsByTagName("item");
				var x = 0;
				var doctitle = doc.evaluate("//channel/title/text()").item(0).nodeValue;
				for(var c = 0; c < items.length; c++) {
					var item = items.item(c);

					var title = item.getElementsByTagName("title").item(0).text;
					var row = Ti.UI.createTableViewRow({
						height : 60,
						hasChild : true
					});
					var label = Ti.UI.createLabel({
						text : title,
						left : 5,
						top : 5,
						bottom : 5,
						right : 5,
						font : {
							fontSize : 14
						},
						color : '#000'
					});
					row.add(label);
					data[x++] = row;
					row.shortTitle = title;
					row.url = item.getElementsByTagName("link").item(0).text;
				}
				tableview.setData(data);
				
				ai.hideModal();
				
				tableview.addEventListener('click', function (e) {
					var HtmlWindow = require("ui/HtmlWindow");
					eduCore.controller.open(new HtmlWindow({ url:e.row.url, title:e.row.shortTitle}));
				});
				
			} catch(E) {
				Titanium.API.log(E);
			}
		};

		xhr.send();
	}
	
	
	
	createRss();
	
	return instance;
};
